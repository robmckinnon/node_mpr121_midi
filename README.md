# node_mpr121_midi

Convert MPR122 capacitive touch events into MIDI messages - for running on Raspberry Pi.

## Installation

To install on Raspberry Pi:

First install lib dependencies:

```
sudo apt-get update
sudo apt full-upgrade -y

sudo apt-get install i2c-tools
sudo apt-get install libasound2-dev
```

Install Node if not yet installed:

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Reboot:

```
sudo reboot
```

Download repo and install:

```
git clone https://gitlab.com/robmckinnon/node_mpr121_midi.git
cd node_mpr121_midi

npm install
```
